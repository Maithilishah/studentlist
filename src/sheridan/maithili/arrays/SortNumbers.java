/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan.maithili.arrays;

import java.util.Arrays;

/**
 *
 * @author Admin
 */
public class SortNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] my_array1 = {1456, 1458, 1254, 1472, 1457};
        sortNumbers(my_array1);
    }
    public static void sortNumbers(int[] my_array1) {
    System.out.println("Original numeric array : " +Arrays.toString(my_array1));
    Arrays.sort(my_array1);
    System.out.println("Sorted numeric array : "+Arrays.toString(my_array1));
    
    }
    
}
